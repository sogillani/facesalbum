package com.ztc.faces.album;

import com.ztc.faces.album.utils.FaceDetectionUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {
	
	private static int screenHeight;
	private static int screenWidth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics( dm );
		
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		
		FaceDetectionUtils.initialize(MainActivity.this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_face:
//			Toast.makeText(this, "Add New Face", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(MainActivity.this, FaceDetectionActivity.class);
			intent.putExtra("Training", true);
			MainActivity.this.startActivity(intent);
//			overridePendingTransition(R.anim.slide_inrighttoleft, R.anim.slide_outrighttoleft);
			MainActivity.this.finish();
			break;

		default:
			break;
		}

		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public static int getScreenHeight(){
		return screenHeight;
	}

	/**
	 * 
	 * @return
	 */
	public static int getScreenWidth(){
		return screenWidth;
	}
}
